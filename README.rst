========
exifdate
========

I'm fed up with all 3rd party foto upload apps that set the date of creation as the date of import.

Basta.

I want the original date when the foto was took.

This program prints out the original date from the EXIF information.
