{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy as BL
--import Data.ByteString.Lazy.Char8 (putStrLn)
import qualified Data.ByteString.Lazy.Char8 as C8
import Data.Binary.Get
import Data.Word
--import Data.Char
--import Numeric (showHex)
--import Text.Hexdump
--import Text.Printf (printf, PrintfArg)
import Text.Show.ByteString
import qualified Text.Show.ByteString as SB
--import Data.ByteString.Lazy.Search

import Data.Int
import System.IO (stderr)

deserialiseHeader :: Get (Word16, Word16, Word16, Word32, Word16, Word16)
deserialiseHeader = do
  soi       <- getWord16be -- | EOI marker: 0xFFD8
  app1      <- getWord16be -- | APP0 marker: 0xFFE1
  app1_size <- getWord16be
  exif      <- getWord32be
  _         <- getWord16be
  mm_or_ii  <- getWord16be
  forty_two <- getWord16be
  return (soi, app1, app1_size, exif, mm_or_ii, forty_two)

deserialiseTrailer :: Get Word16
deserialiseTrailer = do getWord16be -- | EOI marker: 0xFFD8
  --eoi       <- getWord16be -- | EOI marker: 0xFFD8
  --return eoi

deserialiseTiff :: Get (Word16, Word16)
deserialiseTiff = do
  --_         <- getWord64be
  _              <- getByteString 12
  be_or_le       <- getWord16be -- | MM/II
  _              <- getWord16be
  zeroth_ifd_off <- getWord16be
  return (be_or_le, zeroth_ifd_off) 

get_block :: Int -> Get ByteString
get_block = getByteString

-- | slice:
-- https://stackoverflow.com/questions/16849443/idiomatic-way-to-take-a-substring-of-a-bytestring
slice :: Int64 -> Int64 -> BL.ByteString -> BL.ByteString
slice start len = BL.take len . BL.drop start

main :: IO ()
main = do
  --putStrLn "'ello world"
  contents <- BL.readFile "IMG_1790.jpg"
  --let soI = (BL.unpack . BL.take 2) contents
  {-
   if BL.take 2 contents == '\xffd8'
    then putStr "soI:*"
         --(C8.putStrLn . BL.take 2) contents
         --putStrLn "*"
    else putStrLn "No soi"
  -}
  --(C8.putStrLn . BL.take 512) contents
  -- | Get the Start Of Image marker, 0(2) bytes.
  -- Then check them.
  let
    (soi, app1, app1_size_w16, exif, mm_or_ii, forty_two) =
      runGet deserialiseHeader contents

  let app1_size = fromIntegral app1_size_w16
  --let alen = runGet deserialiseHeader contents
  --SB.print $ showHex "10"  $ (runPut. showp) alen
  case soi of
    65496 -> print_stuff "D:80:have soi:*" soi -- | SOI marker (0xFFD8), in decimal
    _     -> C8.hPutStrLn stderr "D:81:No soi"

  -- | Check for the EOI: 0xFFD9 is the image/file end marker
  let eoi = runGet deserialiseTrailer $ C8.reverse contents
  case eoi of
    55807 -> print_stuff "D:86:have eoi:*" eoi -- | EOI marker (0xFFD9), decimal in reverse
    _     -> C8.hPutStrLn stderr "D:87:No eoi"
  
  case app1 of
    65505 -> do print_stuff "D:90:have app1:*" app1 -- | APP1 marker (0xFFE1), in decimal
            --print_stuff "D:86:app1 size:*" app1_size
    _     -> C8.hPutStrLn stderr "D:92:No app1"
  
  -- | non funziona, adesso funziona!
  let app1_block = runGet (get_block (app1_size + 2)) contents

  -- | Check for 42 in pos. immediately after 'MM'
  --case zeroth_ifd_off of
    --42 -> print_stuff "D:112:have 42: " zeroth_ifd_off -- | 'Exif' marker 0x45766966, in decimal
    --_  -> C8.hPutStrLn stderr "D:113:No 42"

  {- | Get app1_size bytes from contents
  let app1_body = C8.take app1_size $ C8.drop 2 contents
  --print_stuff "app1_body" app1_body
  C8.hPutStrLn stderr "D:114:app1_body:"
  --BL.putStr $ (runPut . showp) app1_body
  --C8.putStr app1_body
  -}

  -- | Get 'Exif' from the stream
  case exif of
    1165519206 -> print_stuff "D:112:have 'Exif':*" exif -- | 'Exif' marker 0x45766966, in decimal
    _          -> C8.hPutStrLn stderr "D:113:No 'Exif'"

  -- | Check for Motorola ('MM', be) or Intel ('II', le) from the stream
  case mm_or_ii of
    19789     -> print_stuff "D:117:have 'MM':*" mm_or_ii  -- | 'MM' marker (0x4d4d)
    18761     -> print_stuff "D:118:have 'II':*" mm_or_ii     -- | 'II' marker (0x4949)
    _         -> C8.hPutStrLn stderr "D:119:No 'MM' or 'II markers - problem!"

  -- | Check for the TIFF head marker: MM
  let (be_or_le, zeroth_ifd_off) = runGet deserialiseTiff contents
  case be_or_le of
    19789 -> print_stuff "D:124:have be_or_le: " be_or_le -- | 'MM' marker (0x4d4d)
    18761 -> print_stuff "D:125:have be_or_le: " be_or_le -- | 'II' marker (0x4949)
    _     -> print_stuff "D:126:non ce be_or_le: " be_or_le

  case forty_two of
    42 -> C8.hPutStrLn stderr "D:129:have '42' after BE/LE marker"
    _     -> C8.hPutStrLn stderr "D:130:there is no '42' after BE/LE marker"

  {-if mm_or_ii == 19789         -- | 'MM' marker (0x4d4d)
    then print_stuff "have 'MM':*" mm_or_ii
    else
      if mm_or_ii == 18761         -- | 'II' marker (0x4949)
        then print_stuff "have 'II':*" mm_or_ii
        else
          C8.hPutStrLn stderr "No 'MM' or 'II markers - problem!"
  -}
  -- | use slice from:
  -- get the length of the TIFF header
  -- NF print_stuff "" $ slice 5 2 contents


  return ()

-- | Return hex characters for the byte value.
print_stuff ::  SB.Show a => String -> a -> IO ()
print_stuff b4 what = do
  C8.hPutStr stderr $ C8.pack b4
  C8.putStr $ (runPut . showp) what
  C8.hPutStrLn stderr "*"

-- | Return hex characters for the byte value.
-- Author: http://hackage.haskell.org/package/hexdump
--byteToHex :: (Text.Printf.PrintfArg a) => a -> ByteString
--byteToHex :: Word8 -> ByteString
--byteToHex = printf "%02x"


